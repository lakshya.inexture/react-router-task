import React from "react";
import { Route, Routes } from "react-router-dom";
import Dashboard from "./components/Private Routes/Dashboard";
import Home from "./components/Public Routes/Home";
import Login from "./components/Public Routes/Login";
import Navbar from "./components/Navbar";
import Register from "./components/Public Routes/Register";
import PrivateRoutes from "./components/Private Routes/PrivateRoutes";

import "./App.css";

function App() {
    return (
        <div className="App">
            <Navbar />
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/register" element={<Register />} />
                <Route path="/login" element={<Login />} />
                {/* Private Routes */}
                <Route element={<PrivateRoutes />}>
                    <Route path="/dashboard" element={<Dashboard />} />
                </Route>
            </Routes>
        </div>
    );
}

export default App;
