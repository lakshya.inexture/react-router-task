import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { decryptData } from "../../helper/helper";

const Login = () => {
    const navigate = useNavigate();
    // react-router function navigate to a diff page

    const [loginData, setLoginData] = useState({ email: "", password: "" });
    // used state to compare login details against local storage

    useEffect(() => {
        localStorage.getItem("flag") === "true" && navigate("/dashboard");
        // if logged in, redirects to dashboard
    }, []);

    let decrypt;
    let localData;
    // initialised variables so that we can use these to import encrypted data from local storage and use decrypt helper to decrypt

    if (localStorage.getItem("userData")) {
        localData = localStorage.getItem("userData").toString();
        decrypt = decryptData(localData);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (decrypt === undefined) {
            return alert("Invalid email or password");
            // if there is no saved data, throws alert
        }
        if (
            loginData.email === decrypt.email &&
            loginData.password === decrypt.password
        ) {
            localStorage.setItem("flag", true);
            navigate("/dashboard");
            // logs in if correct values found and navigates to dashboard
        } else {
            alert("Invalid email or password");
            // alert for wrong credentials
        }
    };

    return (
        <div className="login-wrapper">
            <div className="login">
                <form>
                    <input
                        type="text"
                        placeholder="Enter your email"
                        value={loginData.email}
                        onChange={(e) =>
                            setLoginData({
                                ...loginData,
                                email: e.target.value,
                            })
                        }
                    />
                    <input
                        type="password"
                        placeholder="Enter your password"
                        value={loginData.password}
                        onChange={(e) =>
                            setLoginData({
                                ...loginData,
                                password: e.target.value,
                            })
                        }
                    />
                    <input
                        type="submit"
                        value="Login"
                        onClick={(e) => handleSubmit(e)}
                        className="btn btn-primary"
                    />
                </form>
            </div>
        </div>
    );
};

export default Login;
