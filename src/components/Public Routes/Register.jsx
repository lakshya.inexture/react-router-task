import React, { useEffect, useRef } from "react";
import { useForm } from "react-hook-form";
import * as crypto from "crypto-js";
import { useNavigate } from "react-router-dom";

const Register = () => {
    const navigate = useNavigate();
    const {
        register,
        handleSubmit,
        watch,
        formState: { errors },
    } = useForm();
    // react-hook-form functions to match for passwords, handle submit function, use register for validation and display errors

    const onSubmit = (data) => {
        let setData = crypto.AES.encrypt(
            JSON.stringify(data),
            "Imitate-Imitate-Ascension-Duct5-Spinner"
            // string is secret key
        ).toString();
        // encrypts data received after user submits form

        localStorage.setItem("userData", setData);
        localStorage.setItem("flag", false);
        navigate("/login");
        // sets encrypted user data to local storage, sets login flag as false and navigates user to login page
    };

    const password = useRef();
    password.current = watch("password");
    // react-hook-form function to match password and confirm password

    useEffect(() => {
        localStorage.getItem("flag") === "true" && navigate("/dashboard");
    }, []);

    return (
        <div className="register-wrapper">
            <div className="register">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <input
                        type="text"
                        {...register("userName", {
                            required: "Please enter your username",
                            minLength: { value: 3, message: "Min length is 3" },
                            pattern: {
                                value: /\w{3}/,
                                message: "Please enter alphabets only",
                            },
                        })}
                        placeholder="Username"
                        autoComplete="off"
                    />
                    <p>{errors.userName?.message}</p>

                    <input
                        type="text"
                        {...register("email", {
                            required: "Please enter your email",
                            pattern: {
                                value: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
                                message: "Please enter a valid email",
                            },
                        })}
                        placeholder="Email"
                        autoComplete="off"
                    />
                    <p>{errors.email?.message}</p>
                    <input
                        type="password"
                        {...register("password", {
                            required: "Please enter your password",
                            pattern: {
                                value: /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,14}$/,
                                message:
                                    "Your password should contain at least a smallcase letter, an uppercase letter, a number and a special character and should be between 8 - 14 characters",
                            },
                        })}
                        placeholder="Password"
                        autoComplete="off"
                    />
                    <p>{errors.password?.message}</p>
                    <input
                        type="password"
                        {...register("confirmPassword", {
                            required: "Please enter your password",
                            pattern: {
                                value: /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,14}$/,
                                message:
                                    "Your password should contain at least a smallcase letter, an uppercase letter, a number and a special character and should be between 8 - 14 characters",
                            },
                            validate: (val) =>
                                val === password.current ||
                                "The passwords do not match",
                        })}
                        placeholder="Confirm Password"
                        autoComplete="off"
                    />
                    <p>{errors.confirmPassword?.message}</p>
                    <input
                        type="text"
                        {...register("phoneNumber", {
                            required: "Please enter your phone number",
                            pattern: {
                                value: /\d{10}/,
                                message: "Please enter a valid phone number",
                            },
                        })}
                        placeholder="Phone Number"
                        autoComplete="off"
                    />
                    <p>{errors.phoneNumber?.message}</p>
                    <input
                        type="text"
                        {...register("city", {
                            required: "Please enter your city",
                            pattern: {
                                value: /[a-zA-Z]{3}/,
                                message: "Please enter a valid city name",
                            },
                        })}
                        placeholder="City"
                        autoComplete="off"
                    />
                    <p>{errors.city?.message}</p>
                    <input
                        type="text"
                        {...register("state", {
                            required: "Please enter your state",
                            pattern: {
                                value: /[a-zA-Z]{3}/,
                                message: "Please enter a valid state name",
                            },
                        })}
                        placeholder="State"
                        autoComplete="off"
                    />
                    <p>{errors.state?.message}</p>

                    <input
                        className="btn btn-outline-light"
                        type="submit"
                        value="Register"
                    />
                </form>
            </div>
        </div>
    );
};

export default Register;
