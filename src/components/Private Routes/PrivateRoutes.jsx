import React from "react";
import { Navigate, Outlet } from "react-router-dom";

const PrivateRoutes = () => {
    const flag = localStorage.getItem("flag");
    return (
        <div>{flag !== "true" ? <Navigate to={"/login"} /> : <Outlet />}</div>
    );
};

export default PrivateRoutes;
