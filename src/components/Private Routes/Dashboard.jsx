/* eslint-disable no-restricted-globals */
import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { decryptData } from "../../helper/helper";

const Dashboard = () => {
    const navigate = useNavigate();
    // react-router function navigate to a diff page

    let decrypt;
    let localData;
    // initialised variables so that we can use these to import encrypted data from local storage and use decrypt helper to decrypt

    if (localStorage.getItem("userData")) {
        localData = localStorage.getItem("userData").toString();
        decrypt = decryptData(localData);
    }

    const handleLogout = () => {
        localStorage.setItem("flag", false);
        navigate("/");
        // sets login flag to false and redirects to home
    };

    const handleDeleteProfile = () => {
        let getConfirm = confirm(
            "Are you sure you want to delete your profile?"
        );
        if (getConfirm) {
            localStorage.setItem("userData", "");
            localStorage.setItem("flag", false);
            navigate("/");
            // confirms user to delete their data, sets login flag as false and navigates them to home page
        }
    };

    return (
        <div>
            {decrypt ? (
                <>
                    <h1>Hello {decrypt.userName}</h1>
                    <h3>Phone: {decrypt.phoneNumber}</h3>
                </>
            ) : (
                <div></div>
            )}
            {/* displays username and phone if found else nothing */}
            <button className="btn btn-primary" onClick={handleLogout}>
                Logout
            </button>
            <button className="btn btn-danger" onClick={handleDeleteProfile}>
                Delete Profile
            </button>
        </div>
    );
};

export default Dashboard;
