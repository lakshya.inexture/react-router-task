import * as crypto from "crypto-js";

// function using crypto-js to decrypt hashed form values

export const decryptData = (localData) => {
    let decrypt = JSON.parse(
        crypto.AES.decrypt(
            localData,
            "Imitate-Imitate-Ascension-Duct5-Spinner"
        ).toString(crypto.enc.Utf8)
    );
    return decrypt;
};
